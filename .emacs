(menu-bar-mode -1)
(toggle-scroll-bar -1)
(tool-bar-mode -1)
(setq inhibit-startup-message t)

(setq-default tab-width 4)
(setq-default indent-tabs-mode nil)

(set-face-attribute 'default nil :family "consolas" :height 120)

(setq make-backup-files nil)
(setq auto-save-default nil)

(setq display-line-numbers-type 'relative)
(global-display-line-numbers-mode)


;; Package manager MELPA
(require 'package)
(add-to-list 'package-archives '("melpa" . "https://melpa.org/packages/") t)
(package-initialize)

;; Quick cd
(setenv "pd" "D:/Projects")

;; Common keybinds
(global-set-key (kbd "C-M-7") "{")
(global-set-key (kbd "C-M-0") "}")
(global-set-key (kbd "C-M-8") "[")
(global-set-key (kbd "C-M-9") "]")

;; C mode settings
(setq c-basic-offset 4)
(setq c-syntactic-indentation nil)


;; Themes

(add-to-list 'custom-theme-load-path "~/.emacs.d/themes/")
(load-theme 'tron-legacy t)


(string-match "system32" "C:/WINDOWS/system32")

(if (string-match "system32" default-directory)
    (cd "~/"))


;; Commands

(fmakunbound 'test)
(defun term (buffer)
  (interactive "sBuffer name (default cmd): ")
  ;;(message "%i" (length buffer))
  (if (= (length buffer) 0)
      (setq buffer "cmd"))
  (let ((buffer (concat "*t-" buffer)))
    (make-comint-in-buffer "cmd" buffer "cmd" nil)
    (pop-to-buffer buffer)))




;; Notes

(with-current-buffer (find-file "~/.notes.org")
  (rename-buffer "*notes*"))
